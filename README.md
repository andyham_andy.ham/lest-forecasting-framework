# LEST forecasting framework

#### Project Profile
The fluctuation of an exchange rate is easily influenced by complex factors such as the country's economic fundamentals, international trade, and political relations. Since the beginning of the trade war between the Sino-U.S., the Renminbi (RMB) exchange rate has experienced significant fluctuations. The changes in the economic and trade relationship between the two countries have introduced a series of uncertainties into the forecast of the RMB exchange rate.
Based on this background, we innovatively incorporate Sino-US trade events as a significant feature in exchange rate forecasting. Firstly, In the data phase, we leverage the situational learning capabilities of Large Lange Models (LLM), specifically ChatGPT, combined with prompt engineering to enable ChatGPT to provide real-time, online, and precise automated sentiment analysis of Sino-US trade events. 
Breaks through the performance limitations of sentiment analysis in terms of lexicon size and model iteration in existing mainstream exchange rate forecasting methods. EMD was also used to enhance the dimensionality of the exchange rate data and increase the feature interactions. 
Secondly, in the modeling phase, we propose the SG-TL fusion model. This model integrates STGCN and GRU models in the feature processing approach to extract exchange rate features more accurately, mining temporal and spatial features within exchange rate data. 
Finally, a collaborative approach using Transformer and LSTM models is employed in the forecasting phase to forecast the Sino-US exchange rates. Experiments demonstrate that the exchange rate forecasting framework LEST constructed by LLM-EMD and SG-TL, is able to accurately capture the exchange rate fluctuations during the period of the Sino-US trade war. Its forecasting performance is more precise, efficient, and stable than similar work.

#### Description of the document
1.  The folder  **data**  contains the dataset used in this paper. To replace the exchange rate data and trade event labels, look for the corresponding labels in `data/exchange_rate.csv`.
2.  `main_feature.py`:Code for forecasting based on the LEST framework,  **incorporating trade event labelling in conjunction with exchange rate data** .
3.  `main_non_feature.py`:Code for forecasting based on the LEST framework  **using only exchange rate data** .
4.  `model.py`:Code for the LEST forecasting framework.

#### How to use?

1.  Based on the LEST framework, **Only exchange rate data** are used when making exchange rate forecasts:<br/>
    Please run `main_non_feature.py`<br/>
    Code **at line 62**, replaceable **Exchange rate dataset** .
2.  Based on the LEST framework, **including trade event labelling in conjunction with exchange rate data** when making forecasts:<br/>
    Please run `main_feature.py`.<br/>
    Code **at line 61**, replaceable **Exchange rate dataset**.Code **at line 85**, replaceable **Trade event tag**.

#### FAQ

Q.  How to make the model perform better (achieve the results in the paper or even better?)<br/>
A.  In `main_feature.py`, parameters can be adjusted on lines 92 to 100. In `main_non_feature.py`，parameters can be adjusted on lines 86 to 94.<br/>